#include "vm.h"

#define HEAP_SIZE 64 * 1024 * 1024

static char heap[HEAP_SIZE]
static char* heap_ptr

size_t memcpy(void* dst, const void*src, size_t len){
	size_t i;

	if ((uintptr_t)dst % sizeof(long) == 0 &&
		(uintptr_t)src % sizeof(long) == 0 &&
		len % sizeof(long) == 0){
			long *d = dst;
			const long *s =src;

			for ( i=0;i<len/sizeof(long); i++){
				d[i] = s[i];
			}
	}else{
		char *d = dst;
		const char *s = src;

		for (i=0;i<len;i++){
			d[i] = s[i];
		}
	}
	return 0;
}

char* kmalloc(size_t size){
	ASSERT(heap_ptr + size < heap + HEAP_SIZE, "kmalloc failed, out of memory");

	char* ret = heap_ptr;
	heap_ptr += size;
	return ret;
}