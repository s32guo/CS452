#ifndef __VM_H__
#define __VM_H__

size_t memcpy(void* dst, const void* src, size_t len);

char* kmalloc(size_t size);

#endif