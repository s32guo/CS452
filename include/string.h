#ifndef __STRING_H__
#define __STRING_H__

#define DEFAULT_MAX 256

typedef struct string{
	char* chars;
	int max;
	int size;
}string;

void str_create(char* chars, string *s, int length);

void str_cpy(string* s1, string* s2);

void str_char(string* s);

void str_length(string* s);

#endif