#ifndef __LIB_H__
#define __LIB_H__

int abs(int n);

int atoi(char* in);

int max(int m, int n);

int min(int m, int n);

#endif