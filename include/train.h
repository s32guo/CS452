#ifndef __TRAIN_H__
#define __TRAIN_H__

void tr_init();

void tr_set_speed(int tr_num, int speed);

void tr_rv(int tr_num);

void tr_sw(int sw_num, int direction);

#endidf