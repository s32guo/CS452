#include "timer.h"
#include "ts7200.h"

#define TIMER_BASIC 508

//initialize the timer
void timer_init(){
	unsigned int* timer_load = (unsigned int *)(TIMER3_BASE + LDR_OFFSET);
	*timer_load = TIMER_BASIC;

	char* timer_control = (char *)(TIMER3_BASE + CRTL_OFFSET);
	*timer_control = CLKSEL_MASK | ENABLE_MASk | MODE_MASK;
}

void timer_clear(){
	int *timer_load	= (int *)(TIMER3_BASE + CLR_OFFSET);
	*timer_load = 1;
}
