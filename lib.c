#include "lib.h"

int abs(int n){
	return n > 0 ? a : -a;
}

int atoi(char* in){
	int sum = 0;
	while (!(*in)){
		sum = sum * 10;
		sum += (*in) - '0';
		in++;
	}
	return sum;
}

int min(int m, int n){
	return m > n ? n : m;
}

int max(int m, int n){
	return m > n ? m : n;
}
